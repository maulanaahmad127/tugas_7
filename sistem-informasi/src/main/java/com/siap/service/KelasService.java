package com.siap.service;

import java.util.Optional;

import com.siap.model.Kelas;
import com.siap.repo.KelasRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KelasService {
    
    @Autowired
    private KelasRepo repo;

    public Iterable<Kelas> findAll(){
        return repo.findAll();
    }

    public Iterable<Integer> findAllSks(){
        return repo.getTotalSksByName();
    }

    public Iterable<String> findAllName(){
        return repo.getNamaByName();
    }

    public void add(Kelas kelas){
         repo.save(kelas);
    }

    public Optional<Kelas> findById(int mahasiswa_id, int matkul_id){
        return repo.findKelasByMahasiswaId(mahasiswa_id, matkul_id);
    }

    public void update(Kelas kelas){
        repo.save(kelas);
    }

    public void deleteById(int mahasiswa_id, int matkul_id){
        repo.deleteByMahasiswaId(mahasiswa_id, matkul_id);
    }
}
