package com.siap.service;

import java.util.Optional;

import com.siap.model.MataKuliah;
import com.siap.repo.MataKuliahRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MataKuliahService {
    
    @Autowired
    private MataKuliahRepo repo;

    public Iterable<MataKuliah> findAll(){
        return repo.findAll();
    }

    public void add(MataKuliah mataKuliah){
         repo.save(mataKuliah);
    }

    public Optional<MataKuliah> findById(int id){
        return repo.findById(id);
    }

    public void update(MataKuliah mataKuliah){
        repo.save(mataKuliah);
    }

    public void deleteById(int id){
        repo.deleteById(id);
    }
}
