package com.siap.service;

import java.util.Optional;

import com.siap.model.Mahasiswa;
import com.siap.repo.MahasiswaRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MahasiswaService {
    
    @Autowired
    private MahasiswaRepo repo;

    public Iterable<Mahasiswa> findAll(){
        return repo.findAll();
    }

    public void add(Mahasiswa mahasiswa){
         repo.save(mahasiswa);
    }

    public Optional<Mahasiswa> findById(int id){
        return repo.findById(id);
    }

    public void update(Mahasiswa mahasiswa){
        repo.save(mahasiswa);
    }

    public void deleteById(int id){
        repo.deleteById(id);
    }
}
