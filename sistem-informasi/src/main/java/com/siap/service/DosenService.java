package com.siap.service;

import java.util.Optional;

import com.siap.model.Dosen;
import com.siap.repo.DosenRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DosenService {
    
    @Autowired
    private DosenRepo repo;

    public Iterable<Dosen> findAll(){
        return repo.findAll();
    }

    public void add(Dosen dosen){
         repo.save(dosen);
    }

    public Optional<Dosen> findById(int id){
        return repo.findById(id);
    }

    public void update(Dosen dosen){
        repo.save(dosen);
    }

    public void deleteById(int id){
        repo.deleteById(id);
    }
}
