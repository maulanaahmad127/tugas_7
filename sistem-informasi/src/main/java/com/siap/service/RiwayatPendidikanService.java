package com.siap.service;

import java.util.Optional;

import com.siap.model.RiwayatPendidikan;
import com.siap.repo.RiwayatPendidikanRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RiwayatPendidikanService {

    @Autowired
    private RiwayatPendidikanRepo repo;

    public Iterable<RiwayatPendidikan> findAll(){
        return repo.findAll();
    }

    public void add(RiwayatPendidikan rp){
         repo.save(rp);
    }

    public Optional<RiwayatPendidikan> findById(int id){
        return repo.findById(id);
    }

    public void update(RiwayatPendidikan rp){
        repo.save(rp);
    }

    public void deleteById(int id){
        repo.deleteById(id);
    }
    
}
