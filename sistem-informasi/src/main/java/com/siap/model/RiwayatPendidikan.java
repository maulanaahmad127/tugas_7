package com.siap.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class RiwayatPendidikan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String strata;

    private String jurusan;

    private String sekolah;

    private int tahun_mulai;

    private int tahun_selesai;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dosen_id")
    private Dosen dosen;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStrata() {
        return strata;
    }

    public void setStrata(String strata) {
        this.strata = strata;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public String getSekolah() {
        return sekolah;
    }

    public void setSekolah(String sekolah) {
        this.sekolah = sekolah;
    }

    public int getTahun_mulai() {
        return tahun_mulai;
    }

    public void setTahun_mulai(int tahun_mulai) {
        this.tahun_mulai = tahun_mulai;
    }

    public int getTahun_selesai() {
        return tahun_selesai;
    }

    public void setTahun_selesai(int tahun_selesai) {
        this.tahun_selesai = tahun_selesai;
    }

    public Dosen getDosen() {
        return dosen;
    }

    public void setDosen(Dosen dosen) {
        this.dosen = dosen;
    }

    
    
}
