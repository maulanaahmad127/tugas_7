package com.siap.model;

import java.io.Serializable;

public class KelasId implements Serializable{
    private int mahasiswa;
    private int matakuliah;
    
    public int getMahasiswa() {
        return mahasiswa;
    }
    public void setMahasiswa(int mahasiswa) {
        this.mahasiswa = mahasiswa;
    }
    public int getMatakuliah() {
        return matakuliah;
    }
    public void setMatakuliah(int matakuliah) {
        this.matakuliah = matakuliah;
    }

    

}
