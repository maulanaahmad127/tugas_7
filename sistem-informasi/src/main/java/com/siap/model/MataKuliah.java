package com.siap.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;


@Entity
public class MataKuliah {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String nama;

    
    
    private int sks;

    @OneToMany(mappedBy = "matakuliah")
    private List<Kelas> MahasiswaAssoc;

    @ManyToMany(mappedBy = "matkuls" ,cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private Set<Dosen> dosens;
    
    
    public List<Kelas> getMahasiswaAssoc() {
        return MahasiswaAssoc;
    }

    public void setMahasiswaAssoc(List<Kelas> mahasiswaAssoc) {
        MahasiswaAssoc = mahasiswaAssoc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getSks() {
        return sks;
    }

    public void setSks(int sks) {
        this.sks = sks;
    }

    public Set<Dosen> getDosens() {
        return dosens;
    }

    public void setDosens(Set<Dosen> dosens) {
        this.dosens = dosens;
    }

    public String toString() {
        return this.nama;
    }
    
}
