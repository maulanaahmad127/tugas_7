package com.siap.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.JoinColumn;

@Entity
public class Dosen {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String nama;

    private String NIP;

    private String gelar;

    @OneToMany(mappedBy = "dosen", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private List<RiwayatPendidikan> riwayatPendidikan;

    @ManyToMany( fetch = FetchType.LAZY)
    @JoinTable(name = "Dosen_MataKuliah",
    joinColumns = @JoinColumn(name = "dosen_id"),
    inverseJoinColumns = @JoinColumn(name = "matkul_id")
    )
    private Set<MataKuliah> matkuls;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNIP() {
        return NIP;
    }

    public void setNIP(String nIP) {
        NIP = nIP;
    }

    public String getGelar() {
        return gelar;
    }

    public void setGelar(String gelar) {
        this.gelar = gelar;
    }

    public List<RiwayatPendidikan> getRiwayatPendidikan() {
        return riwayatPendidikan;
    }

    public void setRiwayatPendidikan(List<RiwayatPendidikan> riwayatPendidikan) {
        this.riwayatPendidikan = riwayatPendidikan;
    }

    public Set<MataKuliah> getMatkuls() {
        return matkuls;
    }

    public void setMatkuls(Set<MataKuliah> matkuls) {
        this.matkuls = matkuls;
    }

    

}
