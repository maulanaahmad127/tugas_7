package com.siap.controller;

import com.siap.model.Dosen;
import com.siap.model.RiwayatPendidikan;
import com.siap.service.DosenService;
import com.siap.service.MataKuliahService;
import com.siap.service.RiwayatPendidikanService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class DosenController {
    
    @Autowired
    private DosenService dosenService;

    @Autowired
    private RiwayatPendidikanService rpService;

    @Autowired
    private MataKuliahService matkulService;

    @GetMapping("/viewDosen")
    public String view(Model model){
        model.addAttribute("dosen", dosenService.findAll());
        model.addAttribute("riwpen", rpService.findAll());
        return "indexDosen";
    }

    @GetMapping("/addRiwPen")
    public String add(Model model){
        model.addAttribute("dosen", dosenService.findAll());
        model.addAttribute("riwpen", new RiwayatPendidikan());
        return "addRiwPen";
    }

    @PostMapping("/saveRiwPen")
    public String saveRiwPen(Model model, RiwayatPendidikan riwayatPendidikan){
        rpService.add(riwayatPendidikan);
        return "redirect:/viewDosen";
    }

    @GetMapping("/addDosen")
    public String addDosen(Model model){
        model.addAttribute("dosen", new Dosen());
        model.addAttribute("matkul", matkulService.findAll());
        return "addDosen";
    }

    @PostMapping("/saveDosen")
    public String saveDosen(Model model, Dosen dosen){
        dosenService.add(dosen);
        return "redirect:/viewDosen";
    }

    @GetMapping("/deleteDosen/{id}")
    public String deleteDosen(@PathVariable("id") int id){
        dosenService.deleteById(id);
        return "redirect:/viewDosen";
    }

    @GetMapping("/updateDosen/{id}")
    public String updateDosen(@PathVariable("id") int id, Model model){
        model.addAttribute("dosen", dosenService.findById(id));
        model.addAttribute("matkul", matkulService.findAll());
        return "editDosen";
    }

    @PostMapping("/updateDosen")
    public String updatedDosen( Model model, Dosen dosen){
        dosenService.update(dosen);
        return "redirect:/viewDosen";
    }

    @GetMapping("/deleteRiwPen/{id}")
    public String deleteRiwPen(@PathVariable("id") int id){
        rpService.deleteById(id);
        return "redirect:/viewDosen";
    }

    @GetMapping("/updateRiwPen/{id}")
    public String updateRiwPen(@PathVariable("id") int id, Model model){
        model.addAttribute("dosen", dosenService.findAll());
        model.addAttribute("riwpen", rpService.findById(id));
        return "editRiwPen";
    }

    @PostMapping("/updateRiwPen")
    public String updatedRiwPen( Model model, RiwayatPendidikan rp){
        rpService.update(rp);
        return "redirect:/viewDosen";
    }

}
