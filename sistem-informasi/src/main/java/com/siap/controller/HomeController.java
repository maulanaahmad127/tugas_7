package com.siap.controller;

import javax.validation.Valid;

import com.siap.model.Kelas;
import com.siap.model.Mahasiswa;
import com.siap.model.MataKuliah;
import com.siap.service.KelasService;
import com.siap.service.MahasiswaService;
import com.siap.service.MataKuliahService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class HomeController {
    
    @Autowired
    private MahasiswaService mahasiswaService;

    @Autowired
    private MataKuliahService matakuliahService;

    @Autowired
    @Valid
    private KelasService KelasService;


    @GetMapping()
    public String view(Model model, Mahasiswa mahasiswa){
        model.addAttribute("mahasiswa", mahasiswaService.findAll());
        model.addAttribute("matakuliah", matakuliahService.findAll());
        model.addAttribute("kelas", KelasService.findAll());
        model.addAttribute("kelasSKS",  KelasService.findAllSks());
        model.addAttribute("kelasNama", KelasService.findAllName());
        return "index";
    }

    @GetMapping("/addmatkul")
    public String addmatkul(Model model){
        model.addAttribute("matkul", new MataKuliah());
        return "addmatkul";
    }

    @PostMapping("/savematkul")
    public String savematkul(Model model, MataKuliah matakuliah){
        matakuliahService.add(matakuliah);
        return "redirect:/";
    }

    @GetMapping("/addmahasiswa")
    public String addmahasiswa(Model model){
        model.addAttribute("mahasiswa", new Mahasiswa());
        return "addmahasiswa";
    }

    @PostMapping("/savemahasiswa")
    public String savemahasiswa(Model model, Mahasiswa mahasiswa){
        mahasiswaService.add(mahasiswa);
        return "redirect:/";
    }

    @GetMapping("/addkelas")
    public String addkelas(Model model){
        model.addAttribute("kelas", new Kelas());
        model.addAttribute("mahasiswa", mahasiswaService.findAll());
        model.addAttribute("matakuliah", matakuliahService.findAll());
        return "addkelas";
    }

    @PostMapping("/savekelas")
    public String savekelas(RedirectAttributes redatt,@Valid Kelas kelas, Model model){
        try {
            KelasService.add(kelas);
        } catch (Exception e) {
            model.addAttribute("errors", "Error kelebihan Sks");
            System.out.println(e);
            System.out.println("Error kelebihan Sks");
        }
        
        return "redirect:/";
    }

    @GetMapping("/deleteMahasiswa/{id}")
    public String deleteMahasiswa(@PathVariable("id") int id){
        mahasiswaService.deleteById(id);
        return "redirect:/";
    }

    @GetMapping("/updateMahasiswa/{id}")
    public String updateMahasiswa(@PathVariable("id") int id, Model model, Mahasiswa mahasiswa){
        model.addAttribute("mahasiswa", mahasiswaService.findById(id));
        return "editMahasiswa";
    }

    @PostMapping("/updateMahasiswa")
    public String updatedMahasiswa(Model model , Mahasiswa mahasiswa){
        mahasiswaService.update(mahasiswa);
        return "redirect:/";
    }

    @GetMapping("/deleteMatkul/{id}")
    public String deleteMatkul(@PathVariable("id") int id){
        matakuliahService.deleteById(id);
        return "redirect:/";
    }

    @GetMapping("/updateMatkul/{id}")
    public String updateMatakuliah(@PathVariable("id") int id, Model model, MataKuliah matkul){
        model.addAttribute("matkul", matakuliahService.findById(id));
        return "editMatkul";
    }

    @PostMapping("/updateMatkul")
    public String updatedMatkul(Model model , MataKuliah matkul){
        matakuliahService.update(matkul);
        return "redirect:/";
    }

    @GetMapping("/deleteKelas/{mahasiswa_id}/{matakuliah_id}")
    public String deleteKelas(@PathVariable("mahasiswa_id") int mahasiswa_id,@PathVariable("matakuliah_id") int matakuliah_id){
        KelasService.deleteById(mahasiswa_id, matakuliah_id);
        return "redirect:/";
    }

    @GetMapping("/updateKelas/{mahasiswa_id}/{matakuliah_id}")
    public String updateKelas(@PathVariable("mahasiswa_id") int mahasiswa_id,@PathVariable("matakuliah_id") int matakuliah_id , Model model){
        model.addAttribute("kelas", KelasService.findById(mahasiswa_id, matakuliah_id));
        model.addAttribute("mahasiswa", mahasiswaService.findAll());
        model.addAttribute("matakuliah", matakuliahService.findAll());
        return "editKelas";
    }

    @PostMapping("/updateKelas")
    public String updatedKelas(Model model , Kelas kelas){
        KelasService.update(kelas);
        return "redirect:/";
    }
    
}
