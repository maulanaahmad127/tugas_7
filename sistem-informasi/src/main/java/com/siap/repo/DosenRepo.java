package com.siap.repo;

import com.siap.model.Dosen;

import org.springframework.data.repository.CrudRepository;

public interface DosenRepo extends CrudRepository<Dosen, Integer>{
    
}
