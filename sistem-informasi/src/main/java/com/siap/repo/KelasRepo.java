package com.siap.repo;

import java.util.Optional;

import javax.transaction.Transactional;

import com.siap.model.Kelas;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.PathVariable;

public interface KelasRepo extends CrudRepository<Kelas, Integer> {
    
    @Query(value = "select sum(mk.sks) from Kelas as k join MataKuliah mk on k.matakuliah.id = mk.id join Mahasiswa m on k.mahasiswa.id = m.id group by m.nama")
    public Iterable<Integer> getTotalSksByName();

    @Query(value = "select m.nama from Kelas as k join MataKuliah mk on k.matakuliah.id = mk.id join Mahasiswa m on k.mahasiswa.id = m.id group by m.nama")
    public Iterable<String> getNamaByName();


    @Transactional
    @Query(value = "select k from Kelas as k where k.mahasiswa.id = :mahasiswaID and k.matakuliah.id = :matkulID")
    public Optional<Kelas> findKelasByMahasiswaId(@PathVariable("mahasiswaID") int mahasiswaID, @PathVariable("matkulID") int matkulID );

    @Transactional
    @Modifying
    @Query(value = "delete from Kelas k where k.mahasiswa.id = :mahasiswaID and k.matakuliah.id = :matkulID")
    public void deleteByMahasiswaId(@PathVariable("mahasiswaID") int mahasiswaID, @PathVariable("matkulID") int matkulID );
}
