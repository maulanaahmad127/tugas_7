package com.siap.repo;

import com.siap.model.MataKuliah;

import org.springframework.data.repository.CrudRepository;

public interface MataKuliahRepo extends CrudRepository<MataKuliah, Integer> {
    
    
}
