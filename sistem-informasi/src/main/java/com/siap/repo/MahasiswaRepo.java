package com.siap.repo;

import com.siap.model.Mahasiswa;

import org.springframework.data.repository.CrudRepository;

public interface MahasiswaRepo extends CrudRepository<Mahasiswa, Integer> {
    
}
