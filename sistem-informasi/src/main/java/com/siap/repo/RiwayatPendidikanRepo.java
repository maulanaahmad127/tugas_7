package com.siap.repo;

import com.siap.model.RiwayatPendidikan;

import org.springframework.data.repository.CrudRepository;

public interface RiwayatPendidikanRepo extends CrudRepository<RiwayatPendidikan, Integer> {
    
}
