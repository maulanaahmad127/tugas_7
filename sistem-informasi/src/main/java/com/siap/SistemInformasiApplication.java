package com.siap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SistemInformasiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SistemInformasiApplication.class, args);
	}

}
